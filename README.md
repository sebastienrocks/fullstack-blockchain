# Building a blockchain in JS

This was the demo of building a blockchain at FullStack2018 in London, UK.

This contains a (hopefully) working example of a ledger node, a blockchain, and a web server

Note: This code was heavily inspired by [this repo](https://github.com/dbjsdev/BrewChain)

```
Don Burks
don@donburks.com
don@lighthouselabs.ca
@don_burks on Twitter
```
